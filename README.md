# GroweriqSignup

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Run app

First run `npm install` in the app's folder, then run `ng serve` to run the app. Navigate to `http://localhost:4200/`.

## Testing the code challenge

After submitting the new user form, you must return to the index page through the link on the top of the screen, otherwise the app will be reseted and the data will be lost.
