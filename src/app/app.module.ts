import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { DetailsComponent } from './details/details.component';
import { IndexComponent } from './index/index.component';
import { SharedService } from './services/shared.service';

@NgModule({
  declarations: [
    AppComponent,
    DetailsComponent,
    IndexComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularMaterialModule,
    HttpClientModule,
    NgbModule,
    FormsModule
  ],
  providers: [HttpClientModule, SharedService],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule { }
