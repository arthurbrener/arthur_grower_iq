import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { NewUser } from '../new-user.interface';

@Injectable()
export class SharedService {
  constructor(private http: HttpClient) { }

  newUser: NewUser;
  usersList: {}[] = [];

  getUsers(): Observable<any> {
    return this.http.get('http://5ccc5842f47db800140110d0.mockapi.io/users');
  }

  updateUsersList(userObject: NewUser) {
    this.newUser = userObject;
    this.usersList.unshift(userObject);
  }

  getUsersList() {
    return this.newUser;
  }
}
