export interface NewUser {
  type: string;
  name: string;
  address: string;
  phone_number: string;
  gender: string;
  batches_handled: number;
  yield_acquired: number;
  greenhouse_locations: any;
  years_of_experience: string;
  educational_qualification: string;
  inventory_management_certification: string;

}
