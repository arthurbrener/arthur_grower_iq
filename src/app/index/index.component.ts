import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { Router } from "@angular/router";

import { SharedService } from '../services/shared.service';
import { NewUser } from '../new-user.interface';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent {
  userData$;

  constructor(
    private modalService: NgbModal,
    private sharedService: SharedService,
    private route: Router) {

    if (!this.sharedService.usersList.length)
      this.sharedService.getUsers().subscribe((res) => {
        this.sharedService.usersList = res;
        this.userData$ = this.sharedService.usersList;
      })
    else this.userData$ = this.sharedService.usersList;
  }

  title = 'groweriq-signup';

  visibleColumns = ['name', 'phone_number', 'type'];

  newUserObject: NewUser;

  openModal(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'lg' });
  }

  onSubmit(form: NgForm) {
    this.modalService.dismissAll();

    this.sharedService.updateUsersList(form.value);
    this.route.navigate(['details']);
  }
}
