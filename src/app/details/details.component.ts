import { Component, } from '@angular/core';
import { Router } from "@angular/router";

import { SharedService } from '../services/shared.service';
import { NewUser } from '../new-user.interface';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent {
  newUserObject: NewUser;
  constructor(private sharedService: SharedService,
    private route: Router) {
    this.newUserObject = this.sharedService.getUsersList();

    if (this.newUserObject === undefined) this.route.navigate(['']);
  }
}
